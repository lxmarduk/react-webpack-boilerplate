module.exports = {
	entry: ["babel-polyfill", __dirname + "/app/entry.js"],
	output: {
		path: __dirname + "/dist",
		filename: "bundle.js"
	},
	module: {
		loaders: [
			{
				test: /\.js$/,
				loaders: ['react-hot', 'babel'],
				exclude: /node_modules/
			},
			{
				test: /\.css$/,
				loader: "style!css"
			}
		]
	}
}
