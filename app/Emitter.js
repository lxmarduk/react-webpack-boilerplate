function EventHandlers() {}
EventHandlers.prototype = Object.create(null);

function arrayClone(arr) {
	return arr.map(el => el);
}

function spliceOne(list, index) {
	for (var i = index, k = i + 1, n = list.length; k < n; i += 1, k += 1) {
		list[i] = list[k];
	}
	list.pop();
}

function $getMaxListeners(that) {
	if (that._maxListeners === undefined) {
		return Emitter.defaultMaxListeners;
	}
	return that._maxListeners;
}

function emitNone(handler, isFn, self) {
	if (isFn) {
		handler.call(self);
	} else {
		const listeners = arrayClone(handler);
		listeners.forEach( listener => listener.call(self));
	}
}

function emitOne(handler, isFn, self, arg1) {
	if (isFn) {
		handler.call(self, arg1);
	} else {
		const listeners = arrayClone(handler);
		listeners.forEach(listener => listener.call(self, arg1));
	}
}

function emitTwo(handler, isFn, self, arg1, arg2) {
	if (isFn) {
		handler.call(self, arg1, arg2);
	} else {
		const listeners = arrayClone(handler);
		listeners.forEach(listener => listener.call(self, arg1, arg2));
	}
}

function emitThree(handler, isFn, self, arg1, arg2, arg3) {
	if (isFn) {
		handler.call(self, arg1, arg2, arg3);
	} else {
		const listeners = arrayClone(handler);
		listeners.forEach(listener => listener.call(self, arg1, arg2, arg3));
	}
}

function emitMany(handler, isFn, self, ...args) {
	if (isFn) {
		handler.call(self, ...args);
	} else {
		const listeners = arrayClone(handler);
		listeners.forEach(listener => listener.call(self, ...args));
	}
}

function _addListener(target, type, listener, prepend) {
	if (typeof listener != "function") {
		throw new TypeError("\"listener\" argument must be a function");
	}

	let events = target._events;
	let existing;

	if (!events) {
		events = target._events = new EventHandlers();
		target._eventsCount = 0;
	} else {
		if (events.newListener) {
			target.emit("newListener", type, listener.listener ? listener.listener : listener);
			events = target._events;
		}
		existing = events[type];
	}

	if (!existing) {
		existing = events[type] = listener;
		++target._eventsCount;
	} else {
		if (typeof existing === "function") {
			existing = events[type] = prepend ? [listener, existing] : [existing, listener];
		} else {
			if (prepend) {
				existing.unshift(listener);
			} else {
				existing.push(listener);
			}
		}

		if (!existing.warned) {
			let m = $getMaxListeners(target);
			if (m && m > 0 && existing.length > m) {
				existing.warned = true;
				console.warn("Possible Emitter memory leak detected. " +
					`${existing.length} ${type} listeners added. ` +
					"Use Emitter.setMaxListeners() to increase limit");
			}
		}
	}

	return target;
}

function _onceWrap(target, type, listener) {
	let fired = false;
	function g() {
		target.removeListener(type, g);
		if (!fired) {
			fired = true;
			listener.apply(target, arguments);
		}
	}
	g.listener = listener;
	return g;
}

export default class Emitter {
	static defaultMaxListeners = 10;

	constructor() {
		if (!this._events || this._events === Object.getPrototypeOf(this)._events) {
			this._events = new EventHandlers();
			this._eventsCount = 0;
		}

		this._maxListeners = this._maxListeners || undefined;
	}

	setMaxListeners(numListeners) {
		if (typeof numListeners !== "number" || numListeners < 0 || isNaN(numListeners)) {
			throw new TypeError("'numListeners' argument must be a positive number");
		}
		this._maxListeners = numListeners;
		return this;
	}

	getMaxListeners() {
		return $getMaxListeners(this);
	}

	emit(type, ...args) {
		let doError = (type === "error");
		const events = this._events;

		if (events) {
			doError = (doError && events.error == null);
		} else if (!doError) {
			return false;
		}

		if (doError) {
			let er = arguments[1];
			if (er instanceof Error) {
				throw er;
			} else {
				const err = new Error("Uncaught, unspecified \"error\" event. (" + er + ")");
				err.context = er;
				throw err;
			}
			return false;
		}

		const handler = events[type];
		if (!handler) {
			return false;
		}

		const isFn = (typeof handler === "function");
		const len = arguments.length;
		switch (len) {
			case 1:
				emitNone(handler, isFn, this);
				break;
			case 2:
				emitOne(handler, isFn, this, arguments[1]);
				break;
			case 3:
				emitTwo(handler, isFn, this, arguments[1], arguments[2]);
				break;
			case 4:
				emitThree(handler, isFn, this, arguments[1], arguments[2], arguments[3]);
				break;
			default:
				emitMany(handler, isFn, this, ...args);
		}
		return true;
	}

	addListener(type, listener) {
		return _addListener(this, type, listener, false);
	}

	on(type, listener) {
		return _addListener(this, type, listener, false);
	}

	prependListener(type, listener) {
		return _addListener(this, type, listener, true);
	}

	once(type, listener) {
		if (typeof listener !== "function") {
			throw new TypeError("\"listener\" argument must be a function");
		}
		this.on(type, _onceWrap(this, type, listener));
		return this;
	}

	prependOnceListener(type, listener) {
		if (typeof listener !== "function") {
			throw new TypeError("\"listener\" argument must be a function");
		}
		this.prependListener(type, _onceWrap(this, type, listener));
		return this;
	}

	removeListener(type, listener) {
		if (typeof listener !== "function") {
			throw new TypeError("\"listener\" argument must be a function");
		}

		let position, originalListener;

		const events = this._events;
		if (!events) {
			return this;
		}

		const list = events[type];
		if (!list) {
			return this;
		}

		if (list === listener || (list.listener && list.listener === listener)) {
			if (--this._eventsCount === 0) {
				this._events = new EventHandlers();
			} else {
				delete events[type];
				if (events.removeListener) {
					this.emit("removeListener", type, list.listener || listener);
				}
			}
		} else if (typeof list !== "function") {
			position = -1;
			for (let i = list.length; i > 0; --i) {
				if (list[i] === listener || (list[i].listener && list[i].listener === listener)) {
					originalListener = list[i].listener;
					position = i;
					break;
				}
			}

			if (position < 0) {
				return this;
			}

			if (list.length === 1) {
				list[0] = undefined;
				if (--this._eventsCount === 0) {
					this._events = new EventHandlers();
					return this;
				} else {
					delete events[type];
				}
			} else {
				spliceOne(list, position);
			}

			if (events.removeListener) {
				this.emit("removeListener", type, originalListener || listener);
			}
		}
		return this;
	}

	removeAllListeners(type) {
		const events = this._events;

		if (!events) {
			return this;
		}

		if (!events.removeListener) {
			if (arguments.length === 0) {
				this._events = new EventHandlers();
				this._eventsCount = 0;
			} else if (events[type]) {
				if (--this._eventsCount === 0) {
					this._events = new EventHandlers();
				} else {
					delete events[type];
				}
			}
			return this;
		}

		if (arguments.length === 0) {
			const keys = Object.keys(events);
			for (let i = 0, key; i < keys.length; ++i) {
				key = keys[i];
				if (key === "removeListener") {
					continue;
				}
				this.removeAllListeners(key);
			}
			this.removeAllListeners("removeListener");
			this._events = new EventHandlers();
			this._eventsCount = 0;
			return this;
		}

		const listeners = events[type];
		if (typeof listeners === "function") {
			this.removeListener(type, listeners);
		} else if (listeners) {
			do {
				this.removeListener(type, listeners[listeners.length - 1]);
			} while (listeners[0]);
		}

		return this;
	}

	listeners(type) {
		const events = this._events;
		let ret;

		if (!events) {
			ret = [];
		} else {
			let eventListener = events[type];
			if (!eventListener) {
				ret = [];
			} else if (typeof eventListener === "function") {
				ret = [eventListener];
			} else {
				ret = arrayClone(eventListener);
			}
		}

		return ret;
	}

	listenerCount(type) {
		const events = this._events;
		if (events) {
			const eventListener = events[type];
			if (typeof eventListener === "function") {
				return 1;
			} else if (eventListener) {
				return eventListener.length;
			}
		}
		return 0;
	}

	eventNames() {
		return this._eventsCount > 0 ? Reflect.ownKeys(this._events) : [];
	}
}
