import assert from "../assert";
import Store from "./Store";

function abstractMethod(className, method) {
	assert(false, `Subclasses of ${className} must override ${method} with their own implementation.`);
}

/**
 * This is the basic building block of a Flux application. All of your stores
 * should extend this class.
 *
 *   class CounterStore extends ReduceStore<number> {
 *     getInitialState(): number {
 *       return 1;
 *     }
 *
 *     reduce(state: number, action: Object): number {
 *       switch(action.type) {
 *         case: 'add':
 *           return state + action.value;
 *         case: 'double':
 *           return state * 2;
 *         default:
 *           return state;
 *       }
 *     }
 *   }
 */

export default class ReduceStore extends Store {
	constructor(dispatcher) {
		super(dispatcher);
		this._state = this.getInitialState();
	}

	getState() {
		return this._state;
	}

	getInitialState() {
		return abstractMethod("ReduceStore", "getInitialState");
	}

	reduce(state, action) {
		return abstractMethod("ReduceStore", "reduce");
	}

	areEqual(one, two) {
		return one === two;
	}

	__invokeOnDispatch(action) {
		this.__changed = false;

		const startingState = this._state;
		const endingState = this.reduce(startingState, action);

		assert(endingState !== undefined,
			`${this.constructor.name} returned undefined from reduce(...), did you forget to return state in default case? (use null if this was intentional)`)

		if (!this.areEqual(startingState, endingState)) {
			this._state = endingState;
			this.__emitChange();
		}

		if (this.__changed) {
			this.__emitter.emit(this.__changeEvent);
		}
	}
}
