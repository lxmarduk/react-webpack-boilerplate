import assert from "../assert";
import Emitter from "../Emitter";

export default class Store {
	constructor(dispatcher) {
		this.__className = this.constructor.name;

		this.__charged = false;
		this.__changeEvent = "change";
		this.__dispatcher = dispatcher;
		this.__emitter = new Emitter();
		this._dispatchToken = dispatcher.register((payload) => {
			this.__invokeOnDispatch(payload);
		});
	}

	addListener(callback) {
		return this.__emitter.addListener(this.__changeEvent, callback);
	}

	getDispatcher() {
		return this.__dispatcher;
	}

	getDispatchToken() {
		return this._dispatchToken;
	}

	hasChanged() {
		assert(this.__dispatcher.isDispatching(),
			`${this.__className}.hasChanged(): Must be invoked while dispatching.`);
		return this.__charged;
	}

	__emitChange() {
		assert(this.__dispatcher.isDispatching(),
			`${this.__className}.__emitChange(): Must be invoked while dispatching.`);
		this.__changed = true;
	}

	__invokeOnDispatch(payload) {
		this.__changed = false;
		this.__onDispatch(payload);
		if (this.__changed) {
			this.__emitter.emit(this.__changeEvent);
		}
	}

	__onDispatch(payload) {
		assert(false,
			`${this.__className} has not overriden Store.__onDispatch(), which is required.`);
	}
}
