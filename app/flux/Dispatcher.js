'use strict';

import assert from "../assert";

function* idGenerator() {
	let id = 0;
	while (true) {
		yield "ID_" + (id++);
	}
}

export default class Dispatcher {
	constructor() {
		this._idGenerator = idGenerator();
		this._callbacks = new Map();
		this._isDispatching = false;
		this._isHandled = new Map();
		this._isPending = new Map();
		this._registerQueue = [];
		this._unregisterQueue = [];
	}

	register(payload) {
		const id = this._idGenerator.next().value;
		if (this._isDispatching) {
			const queueItem = {
				key: id,
				payload: payload
			};
			this._registerQueue.push(queueItem);
		} else {
			this._callbacks.set(id, payload);
		}

		return id;
	}

	unregister(id) {
		if (!this._isDispatching) {
			if (this._callbacks.has(id)) {
				this._callbacks.delete(id);
			}
		} else {
			this._unregisterQueue.push(id);
		}
	}

	waitFor(ids) {
		for (let i = 0; i < ids.length; ++i) {
			const id = ids[i];
			if (this._isPending.has(id)) {
				assert(this._isHandled.has(id), "Dispatcher.waitFor(...): Circular dependency detected while waiting for " + id);
				continue;
			}
			if (this._callbacks.has(id)) {
				this._invokeCallback(id);
			}
		}
	}

	dispatch(payload) {
		if (!this._isDispatching) {
			this._startDispatching(payload);
			try {
				this._callbacks.forEach((_, key) => {
					if (!this._isPending.has(key)) {
						this._invokeCallback(key);
					}
				});
			} finally {
				this._stopDispatching();
			}
		}
	}

	isDispatching() {
		return this._isDispatching;
	}

	_invokeCallback(id) {
		this._isPending.set(id, true);
		this._callbacks.get(id)(this._pendingPayload);
		this._isHandled.set(id, true);
	}

	_startDispatching(payload) {
		this._callbacks.forEach((_, key) => {
			this._isPending.delete(key);
			this._isHandled.delete(key);
		});
		this._pendingPayload = payload;
		this._isDispatching = true;
	}

	_stopDispatching() {
		delete this._pendingPayload;
		this._isDispatching = false;
	}
}
