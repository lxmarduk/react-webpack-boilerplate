export default function assert(condition, errorMessage) {
	"use strict";
	if (!condition) {
		if (typeof errorMessage === "string") {
			errorMessage = new Error(errorMessage);
		}

		errorMessage.name = "Assert Violation";
		errorMessage.framesToPop = 1;
		throw errorMessage;
	}
}
