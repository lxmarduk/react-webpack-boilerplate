import React from 'react';
import ReactDOM from 'react-dom';

import Dispatcher from './flux/Dispatcher';

class AppDispatcher extends Dispatcher {
	constructor() {
		super();
	}
}

class App extends React.Component {

	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		return (
			<div>Hello, world!</div>
		);
	}
}

let app = ReactDOM.render(<App />, document.getElementById("app"));
